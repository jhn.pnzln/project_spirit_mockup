import { Component } from "react";
import { Route, BrowserRouter as Router } from 'react-router-dom'

import DashboardPage from '../src/components/pages/dashboard'
export default class App extends Component {

  render() {
    return (
      <Router>
        <div className="App">
          <Route exact path='/' component={(props) =>
            <DashboardPage {...props}/>
          }/>
        </div>
      </Router>
    )
  }
}