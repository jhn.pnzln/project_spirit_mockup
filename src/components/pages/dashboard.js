import { Component } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons'

export default class Dashboard extends Component {
    constructor() {
        super()
        this.state = {
            services: [
                {
                    status: 'Fixed Wireless',
                    speed: '250 / 250 Mbps',
                    description: 'Only $350 per month + GST on a 48 month plan',
                    dataPlans: [
                        'Unlimited Data',
                        'Item 01',
                        'Item 02',
                        'Item 03',
                        'Item 04',
                        'Unlimited Data',
                        'Item 01',
                        'Item 02',
                        'Item 03',
                        'Item 04'
                    ]
                },
                {
                    status: 'Fixed Wireless',
                    speed: '250 / 250 Mbps',
                    description: 'Only $350 per month + GST on a 48 month plan',
                    dataPlans: [
                        'Unlimited Data',
                        'Item 01',
                        'Item 02',
                        'Item 03',
                        'Item 04',
                        'Unlimited Data',
                        'Item 01',
                        'Item 02',
                        'Item 03',
                        'Item 04'
                    ]
                },
            
            ],
        }
    }

    render () {
        return (
            <div className="dashboard-container justify-content-center">
                <div className="row">
                    <div className="col-md-12">
                        <div className="map-holder">
                            <div className="card">
                                <div className="card-body card-map-container">
                                    <div className="col-md-6">
                                        <div className="card-search-bar-container m-5">
                                            <h2 className="card-heading">Select Address</h2>
                                            <FontAwesomeIcon icon={faMapMarkerAlt}  className="search-icon"/>
                                            <input type="text" className="form-control card-search-iput mb-3" placeholder="Enter your address"/>
                                            <a href="#" className="card-anchor-button">Enter Address Manually</a>                                 
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-12 col-sm-12 col-md-3 mt-5">
                        <div className="card">
                            <div className="card-header pt-4 px-4">
                                <h4 className="card-heading">What are you looking for?</h4>
                            </div>
                            <div className="card-body px-4">
                                <div className="form-group mb-5">
                                    <label>Contract Length (Months)</label>
                                    <div className="range-label-container">
                                        <label for="customRange1" class="range-label text-muted">1 Month</label>
                                        <label for="customRange1" class="range-label text-muted">12 Months</label>
                                    </div>
                                    
                                    <input type="range" class="form-range" min="0" max="5" id="contractLength"></input>
                                </div>

                                <div className="form-group mb-5">
                                    <label>Download Speed (Mbps)</label>
                                    <div className="range-label-container">
                                        <label for="customRange1" class="range-label text-muted">100 Mbps</label>
                                        <label for="customRange1" class="range-label text-muted">1 Gbps</label>
                                    </div>
                                    
                                    <input type="range" class="form-range" min="0" max="5" id="downloadSpeed"></input>
                                </div>

                                <div className="form-group mb-5">
                                    <label>Max Price</label>
                                    <div className="range-label-container">
                                        <label for="customRange1" class="range-label text-muted">$100</label>
                                        <label for="customRange1" class="range-label text-muted">$1000</label>
                                    </div>
                                    
                                    <input type="range" class="form-range" min="0" max="5" id="maxPrice"></input>
                                </div>

                                <div className="form-group mb-4">
                                    <label className="text-muted">Showing</label> <b>4</b> <label className="text-muted">products out of</label> <b>12</b>
                                </div>
                            </div>

                            <div className="card-footer pb-4 px-4">
                                <div className="form-group">
                                    <button type="button" className="btn btn-outline-secondary btn-lg">
                                        <label>Reset Filters</label>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div className="col-xs-12 col-sm-12 col-md-9">
                        {
                            this.state.services.map((service) => {
                                return(
                                    <div className="card card-left-border mt-5">
                                        <div className="card-body data-plans-container p-5">
                                            <div className="card-title">
                                                <label className="card-badge">{service.status}</label>
                                                <h1 className="card-heading large-heading">{service.speed}</h1>
                                                <h4 className="card-heading">Download / Upload</h4>
                                                <p className="large-paragraph">
                                                    {service.description}
                                                </p>
                                            </div>

                                            <div className="card-list">
                                                <h5 className="card-heading">Data Plan</h5>
                                                <ul className="card-ul">
                                                    {
                                                        service.dataPlans.map((data) => {
                                                            return (
                                                                <li className="card-li mb-3">{data}</li>
                                                            )
                                                        })
                                                    }
                                                </ul>
                                            </div>

                                            <div className="card-actions">
                                                <div className="form-group d-grid gap-3 btn-container">
                                                    <button type="button" className="btn btn-outline-secondary btn-lg">
                                                        <label>View Brochure</label>
                                                    </button>

                                                    <button type="button" className="btn btn-dark btn-lg">
                                                        <label>Enquire</label>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
            
        )
    }
}